/*
	Copyright 2020-2021 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client.h"
#include "channel.h"

std::map<std::string,Channel*> Channels;

Channel::Channel(const std::string& ChannelName):
Name(ChannelName)
{
	Channels[Name]=this;
	UserCount=0;
}

const std::string& Channel::GetName(void) const
{
	return Name;
}

unsigned Channel::GetUserCount(void) const
{
	return UserCount;
}

void Channel::SetTopic(const std::string& ChannelTopic,const std::string& Author)
{
	Topic=ChannelTopic;
	std::string Response="332 "+Author+" "+Name+" :"+Topic;
	SendCmd("dchat",Response);
}

void Channel::AddUser(Client* User)
{
	std::string UserName=User->GetName();
	Users[UserName]=User;
	UserCount++;

	std::string NameList="353 "+User->GetName()+" = "+Name+" :";
	for(auto i=Users.begin();i!=Users.end();++i)
	{
		NameList+=i->first;
		NameList+=" ";
	}
	User->SendMessage(UserName,NameList);

	std::string Response="JOIN "+Name;
	SendCmd(UserName,Response);
}

void Channel::RemoveUser(Client* User)
{
	std::string Response="PART "+Name;
	std::string UserName=User->GetName();
	SendCmd(UserName,Response);
	
	Users.erase(UserName);
	UserCount--;
	if(UserCount==0)
	{
		Channels.erase(Name);
		delete this;
	}
}

void Channel::SendMessage(const std::string& Author,const std::string& Message) const
{
	for(auto i=Users.begin();i!=Users.end();++i)
	{
		if(i->first==Author) continue;
		i->second->SendMessage(Author,Message);
	}
}

void Channel::SendCmd(const std::string& Author,const std::string& Message) const
{
	for(auto i=Users.begin();i!=Users.end();++i)
	{
		i->second->SendMessage(Author,Message);
	}
}
