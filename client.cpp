/*
	Copyright 2020-2021 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client.h"
#include "channel.h"

static std::map<std::string,Client*> Clients;

static const std::string ServerName="dchat";

static bool IsChannelName(const std::string& Name)
{
	return ((Name[0]=='#')||(Name[0]=='&'));
}

static Client* SearchUser(const std::string& Name)
{
	auto i=Clients.find(Name);
	if(i==Clients.end()) return nullptr;
	else return i->second;
}

int Client::Main(void)
{
	while(1)
	{
		// read a command
		std::string Message;
		if(Sock->ReadMessage(Message)==-1) return -1;

		size_t CommandEnd=Message.find(' ')+1;
		std::string Command=Message.substr(0,CommandEnd-1);

		// execute the command
		if(Command=="USER")
		{
			std::string Response="001 "+NickName+" :Welcome to the Pirate IRC Network, "+NickName;
			SendMessage(ServerName,Response);
			Response="002 "+NickName+" :Your host is 95.227.91.146, running version dchat";
			SendMessage(ServerName,Response);
			Response="003 "+NickName+" :This server was created <to be implemented>";
			SendMessage(ServerName,Response);
		}
		else if(Command=="NICK")
		{
			std::string Nick=Message.substr(CommandEnd);
			if(SearchUser(Nick)==nullptr)
			{
				NickName=Nick;
				Clients[Nick]=this;
			}
		}
		else if(Command=="QUIT")
		{
			for(auto i=JoinedChannels.begin();i!=JoinedChannels.end();++i)
			{
				i->second->RemoveUser(this);
			}
			Clients.erase(NickName);
			return 0;
		}
		else if(Command=="JOIN")
		{
			std::string ChannelName=Message.substr(CommandEnd);
			if(!IsChannelName(ChannelName)) continue;

			Channel* JoinedChannel=nullptr;
			for(auto i=Channels.begin();i!=Channels.end();++i)
			{
				if(i->first==ChannelName)
				{
					JoinedChannel=i->second;
					break;
				}
			}
			if(JoinedChannel==nullptr) JoinedChannel=new Channel(ChannelName);

			JoinedChannels[ChannelName]=JoinedChannel;
			JoinedChannel->AddUser(this);
		}
		else if(Command=="PART")
		{
			size_t ChannelNameEnd=Message.find(' ',CommandEnd);
			std::string ChannelName=Message.substr(CommandEnd,ChannelNameEnd-CommandEnd);

			if(!IsChannelName(ChannelName)) continue;

			Channel* PartedChannel=SearchJoinedChannel(ChannelName);
			if(PartedChannel!=nullptr)
			{
				JoinedChannels.erase(ChannelName);
				PartedChannel->RemoveUser(this);
			}
		}
		else if(Command=="TOPIC")
		{
			size_t NameEnd=Message.find(' ',CommandEnd);
			std::string ChannelName=Message.substr(CommandEnd,NameEnd-CommandEnd);
			std::string ChannelTopic=Message.substr(NameEnd+1);

			if(!IsChannelName(ChannelName));

			Channel* TargetChannel=SearchJoinedChannel(ChannelName);
			if(TargetChannel!=nullptr) TargetChannel->SetTopic(ChannelTopic,NickName);
		}
		else if(Command=="PRIVMSG")
		{
			size_t TargetEnd=Message.find(' ',CommandEnd);
			std::string Target=Message.substr(CommandEnd,TargetEnd-CommandEnd);
			if(IsChannelName(Target))
			{
				Channel* TargetChannel=SearchJoinedChannel(Target);
				if(TargetChannel!=nullptr) TargetChannel->SendMessage(NickName,Message);
			}
			else
			{
				Client* TargetClient=SearchUser(Target);
				if(TargetClient) TargetClient->SendMessage(NickName,Message);
			}
		}
		else if(Command=="LIST")
		{
			for(auto i=Channels.begin();i!=Channels.end();++i)
			{
				std::string ListElement="322 "+NickName+" "+i->first+" "+std::to_string(i->second->GetUserCount());
				SendMessage(ServerName,ListElement);
			}
			std::string ListEnd="323 "+NickName+" :End of /LIST";
			SendMessage(ServerName,ListEnd);
		}
		else if(Command=="PING")
		{
			std::string Response="PONG"+Message.substr(CommandEnd);
			SendMessage(ServerName,Response);
		}
	}
}

void Client::SendMessage(const std::string& Author,const std::string& Message) const
{
	Sock->WriteMessage(":"+Author+" "+Message);
}

Channel* Client::SearchJoinedChannel(const std::string& Name) const
{
	auto i=JoinedChannels.find(Name);
	if(i==JoinedChannels.end()) return nullptr;
	else return i->second;
}

const std::string& Client::GetName(void) const
{
	return NickName;
}
