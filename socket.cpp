/*
	Copyright 2015,2020-2021 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "socket.h"

#include <poll.h>
#include <sys/socket.h>
#include <unistd.h>

Socket::Socket(void)
{
	SockFd=-1;
	AddrStruct.sin_family=AF_INET;
}

int Socket::Create(void)
{
	SockFd=socket(PF_INET,SOCK_STREAM,IPPROTO_IP);
	if(SockFd==-1) return -1;
	return 0;
}

int Socket::Bind(int port)
{
	AddrStruct.sin_port=htons(port);
	AddrStruct.sin_addr.s_addr=htonl(INADDR_ANY);
	return bind(SockFd,(sockaddr*)&AddrStruct,sizeof(AddrStruct));
}

int Socket::Listen(void) const
{
	return listen(SockFd,-1);
}

Socket* Socket::Accept(void) const
{
	struct sockaddr_in tmp;
	socklen_t tmplen=sizeof(tmp);
	int newfd=accept(SockFd,(sockaddr*)&tmp,&tmplen);

	if(newfd==-1) return nullptr;
	Socket* newsock=new Socket();
	newsock->SockFd=newfd;
	return newsock;
}

int Socket::WriteMessage(const std::string& s) const
{
	int ret=send(SockFd,s.c_str(),s.size(),0);
	send(SockFd,"\r\n",2,0);
	return ret;
}

void Socket::WaitForData(void) const
{
	struct pollfd SocketPoll=
	{
		.fd=SockFd,
		.events=POLLIN|POLLHUP,
	};
	poll(&SocketPoll,1,-1);
}

int Socket::ReadMessage(std::string& s) const
{
	while(1)
	{
		int n;
		WaitForData();
		do
		{
			char temp_char;
			n=recv(SockFd,&temp_char,1,MSG_DONTWAIT);
			// il socket è nonbloccante, ergo:
			// se non sono presenti dati recv ritorna -1 e errno=EWOULDBLOCK/EAGAIN
			// se il socket è disconnesso ritorna 0
			if(n==0)
			{
				Close();
				return -1;
			}
			if(temp_char=='\n'&&s.back()=='\r')
			{
				s.pop_back();
				return 0;
			}
			s+=temp_char;
		}while(n!=-1);
	}
}

void Socket::Close(void) const
{
	close(SockFd);
}

Socket::~Socket(void)
{
	Close();
}
