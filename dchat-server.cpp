/*
	dchat-server.cpp

	Copyright 2019-2020 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "channel.h"
#include "client.h"
#include "socket.h"

#include <thread>

std::string ServerStartTime;

static void SpawnClientThread(Socket* s)
{
	Client* ClientObject=new Client(s);
	ClientObject->Main();
	delete ClientObject;
	delete s;
}

int main(int argc,char** argv)
{
	Socket ListeningSocket;
	ListeningSocket.Create();
	if(ListeningSocket.Bind(6667)==-1)
	{
		return -1;
	}
	ListeningSocket.Listen();

	while(1)
	{
		Socket* ClientSocket=ListeningSocket.Accept();
		std::thread ClientThread(&SpawnClientThread,ClientSocket);
		ClientThread.detach();
	}

	return 0;
}
