/*
	Copyright 2015,2020-2021 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

	#include <arpa/inet.h>
	#include <netinet/ip.h>
	#include <string>

	class Socket
	{
		public:
			Socket(void);
			int Create(void);
			int Bind(int port);
			int Listen(void) const;
			Socket* Accept(void) const;
			void WaitForData(void) const;
			int ReadMessage(std::string& s) const;
			int WriteMessage(const std::string& s) const;
			void Close(void) const;
			~Socket(void);
		private:
			int SockFd;
			struct sockaddr_in AddrStruct;
	};
