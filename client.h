/*
	Copyright 2020-2021 Emanuele Davalli <emanuele@davalli.it>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

	#include "socket.h"

	#include <map>
	#include <string>

	class Channel;

	class Client
	{
		public:
			explicit Client(Socket* sock) : Sock(sock){};
			int Main(void);
			void SendMessage(const std::string& Author,const std::string& Message) const;
			Channel* SearchJoinedChannel(const std::string& Name) const;
			const std::string& GetName(void) const;
		private:
			Socket* Sock;
			std::map<std::string,Channel*> JoinedChannels;
			std::string NickName;
	};
