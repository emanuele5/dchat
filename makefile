all: server

server: dchat-server.cpp socket.cpp socket.h client.cpp client.h channel.cpp channel.h
	g++ -Wall -O3 -o dchat-server dchat-server.cpp socket.cpp client.cpp channel.cpp -lpthread
